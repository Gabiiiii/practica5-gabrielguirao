package programa;

import java.util.InputMismatchException;
import java.util.Scanner;
import clases.JuegoNaves;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Programa {
	static Scanner input = new Scanner(System.in);
	static JuegoNaves nuevoJuego = new JuegoNaves();

	public static void main(String[] args) {

		nuevoJuego.altaNaveLider("Mariano Rajoy", 500, 1000, "PP-01");
		nuevoJuego.altaNaveComun(300, 100, "PP-01", 3);
		nuevoJuego.altaBase("Base1");
		nuevoJuego.altaEscuadron("Escuadron del PP", "Azul", 5, "Mariano Rajoy", "PP-01");
		nuevoJuego.asignarEscuadron("Escuadron del PP", "Base1");
		int menu = 0;
		boolean salir = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║        Menu general           ║");
					System.out.println("║ 1-Dar de alta                 ║");
					System.out.println("║ 2-Listar                      ║");
					System.out.println("║ 3-Buscar                      ║");
					System.out.println("║ 4-Eliminar                    ║");
					System.out.println("║ 5-Extras                      ║");
					System.out.println("║ 6-Salir                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				menuAltas();
				break;
			case 2:
				menuListar();
				break;
			case 3:
				menuBuscar();
				break;
			case 4:
				menuEliminar();
				break;
			case 5:
				menuExtras();
				break;
			case 6:
				salir = true;
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Fin del programa               ║");
				System.out.println("╚═══════════════════════════════╝");
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");

			}
		} while (!salir);
		input.close();
	}

	public static void menuAltas() {
		int menu = 0;
		boolean atras = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║         Menu altas            ║");
					System.out.println("║ 1-Dar de alta Naves Lider     ║");
					System.out.println("║ 2-Dar de alta Naves Comunes   ║");
					System.out.println("║ 3-Dar de alta Escuadrones     ║");
					System.out.println("║ 4-Dar de alta Bases           ║");
					System.out.println("║ 5-Atras                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				System.out.println("Introduce el nombre del lider del escuadron");
				String lider = input.nextLine();
				System.out.println("Introduce coste de la nave lider");
				int costeLider = input.nextInt();
				System.out.println("Introduce la vida");
				int vidaLider = input.nextInt();
				input.nextLine();
				System.out.println("Introduce el modelo de la nave");
				String modeloLider = input.nextLine();
				nuevoJuego.altaNaveLider(lider, costeLider, vidaLider, modeloLider);
				break;
			case 2:
				System.out.println("Introduce el coste de la nave");
				int coste = input.nextInt();
				System.out.println("Introduce la vida de la nave");
				int vida = input.nextInt();
				input.nextLine();
				System.out.println("Introduce el modelo de la nave");
				String modelo = input.nextLine();
				System.out.println("Introduce la cantidad de naves que hay");
				int cantidadNaves = input.nextInt();
				nuevoJuego.altaNaveComun(coste, vida, modelo, cantidadNaves);
				break;
			case 3:
				System.out.println("Introduce el nombre del escuadron");
				String nombreEscuadron = input.nextLine();
				System.out.println("Introduce el color del escuadron");
				String colorEscuadron = input.nextLine();
				System.out.println("Introduce el tamanio del escuadron (minimo 2 y maximo 10)");
				int tamanioEscuadron = input.nextInt();
				input.nextLine();
				if (tamanioEscuadron < 2) {
					tamanioEscuadron = 2;
				}
				if (tamanioEscuadron > 10) {
					tamanioEscuadron = 10;
				}
				System.out.println("Introduce el nombre del lider del escuadron");
				String nombreLiderEscuadron = input.nextLine();
				System.out.println("Introduce el modelo del resto de naves del escuadron");
				String modeloNavesEscuadron = input.nextLine();
				nuevoJuego.altaEscuadron(nombreEscuadron, colorEscuadron, tamanioEscuadron, nombreLiderEscuadron,
						modeloNavesEscuadron);
				break;
			case 4:
				System.out.println("Introduce el nombre de la base");
				String nombreBase = input.nextLine();
				nuevoJuego.altaBase(nombreBase);
				break;
			case 5:
				atras = true;
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");
			}
		} while (!atras);
	}

	public static void menuListar() {
		int menu = 0;
		boolean atras = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║        Menu listas            ║");
					System.out.println("║ 1-Listar Naves Lider          ║");
					System.out.println("║ 2-Listar Naves Comunes        ║");
					System.out.println("║ 3-Listar Escuadrones          ║");
					System.out.println("║ 4-Listar Bases                ║");
					System.out.println("║ 5-Atras                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				nuevoJuego.listarNavesLideres();
				break;
			case 2:
				nuevoJuego.listarNavesComunes();
				break;
			case 3:
				nuevoJuego.listarEscuadrones();
				break;
			case 4:
				nuevoJuego.listarBases();
				break;
			case 5:
				atras = true;
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");
			}
		} while (!atras);
	}

	public static void menuBuscar() {
		int menu = 0;
		boolean atras = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║       Menu busquedas          ║");
					System.out.println("║ 1-Buscar Naves Lider          ║");
					System.out.println("║ 2-Buscar Naves Comunes        ║");
					System.out.println("║ 3-Buscar Escuadrones          ║");
					System.out.println("║ 4-Buscar Bases                ║");
					System.out.println("║ 5-Atras                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				System.out.println("Introduce el nombre del lider de la nave");
				String lider = input.nextLine();
				System.out.println(nuevoJuego.buscarNaveLider(lider));
				break;
			case 2:
				System.out.println("Introduce el modelo de la nave");
				String modelo = input.nextLine();
				System.out.println(nuevoJuego.buscarNaveComun(modelo));
				break;
			case 3:
				System.out.println("Introduce el nombre del escuadron");
				String nombreEscuadron = input.nextLine();
				nuevoJuego.buscarEscuadron(nombreEscuadron).mostrarTodo();
				break;
			case 4:
				System.out.println("Introduce el nommbre de la base");
				String nombreBase = input.nextLine();
				nuevoJuego.buscarBase(nombreBase).mostrarTodo();
				break;
			case 5:
				atras = true;
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");
			}
		} while (!atras);
	}

	public static void menuEliminar() {
		int menu = 0;
		boolean atras = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║       Menu eliminar           ║");
					System.out.println("║ 1-Eliminar Naves Lider        ║");
					System.out.println("║ 2-Eliminar Naves Comunes      ║");
					System.out.println("║ 3-Eliminar Escuadrones        ║");
					System.out.println("║ 4-Eliminar Bases              ║");
					System.out.println("║ 5-Atras                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				System.out.println("Introduce el nombre del lider de la nave");
				String lider = input.nextLine();
				nuevoJuego.eliminarNaveLider(lider);
				break;
			case 2:
				System.out.println("Introduce el modelo de la nave");
				String modelo = input.nextLine();
				nuevoJuego.eliminarNaveComun(modelo);
				break;
			case 3:
				System.out.println("Introduce el nombre del escuadron");
				String nombreEscuadron = input.nextLine();
				nuevoJuego.eliminarEscuadron(nombreEscuadron);
				break;
			case 4:
				System.out.println("Introduce el nommbre de la base");
				String nombreBase = input.nextLine();
				nuevoJuego.eliminarBase(nombreBase);
				break;
			case 5:
				atras = true;
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");
			}
		} while (!atras);
	}

	public static void menuExtras() {
		int menu = 0;
		boolean atras = false;
		boolean error = false;
		do {
			do {
				try {
					System.out.println("╔═══════════════════════════════╗");
					System.out.println("║         Menu extras           ║");
					System.out.println("║ 1-Asignar escuadron a base    ║");
					System.out.println("║ 2-Rellenar escuadron con naves║");
					System.out.println("║ 3-Atras                       ║");
					System.out.println("╚═══════════════════════════════╝");
					menu = input.nextInt();
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("Error, debes introducir un numero");
					input.nextLine();
					error = true;
				} catch (Exception e) {
					System.out.println("Error de acceso a la informacion del teclado");
					System.exit(0);
				}
			} while (error);
			switch (menu) {
			case 1:
				System.out.println("Introduce el nombre de la base");
				String nombreBase = input.nextLine();
				System.out.println("Introduce el nombre del escuadron que quieres asignar");
				String nombreEscuadron = input.nextLine();
				nuevoJuego.asignarEscuadron(nombreEscuadron, nombreBase);
				break;
			case 2:
				System.out.println("Introduce el nombre del escuadron a rellenar");
				String nombreEscuadronR = input.nextLine();
				System.out.println("Introduce el nombre del nuevo Lider del escuadron");
				String nombreLider = input.nextLine();
				System.out.println("Introduce el modelo de las nuevas naves del escudron;");
				String modeloNaveComun = input.nextLine();
				nuevoJuego.rellenarEscuadron(nombreEscuadronR, nombreLider, modeloNaveComun);
				break;
			case 3:
				atras = true;
				break;
			default:
				System.out.println("╔═══════════════════════════════╗");
				System.out.println("║Introduce un opcion valida     ║");
				System.out.println("╚═══════════════════════════════╝");
			}
		} while (!atras);
	}

}
