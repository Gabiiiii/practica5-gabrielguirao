package clases;
/**
 * 
 * @author Gabriel Guirao
 *
 */
public final class NaveComun extends Nave {

	private int cantidadNaves;
/**
 * Clase nave comun con un solo constructor
 * @param modelo
 * @param coste
 * @param vida
 * @param cantidadNaves
 */
	public NaveComun(String modelo, int coste, int vida, int cantidadNaves) {
		super(modelo, coste, vida);
		this.cantidadNaves = cantidadNaves;
	}
/**
 * 
 * @return devuelve la cantidad de naves que hay de este tipo
 */
	public int getCantidadNaves() {
		return cantidadNaves;
	}

	public void setCantidadNaves(int cantidadNaves) {
		this.cantidadNaves = cantidadNaves;
	}
/**
 * Metodo para actualizar la cantidad de naves disponibles cada vez que se introducen en un escuadron
 * @param tamano
 */
	public void actualizarCantidadNaves(int tamano) {
		if ((getCantidadNaves() - tamano) >= 0) {
			setCantidadNaves(getCantidadNaves() - tamano);
		} else {
			setCantidadNaves(0);
		}
	}

	@Override
	public String toString() {
		return "Nave Comun: Modelo: " + modelo + ", Coste: " + coste + ", Vida:" + vida;
	}
/**
 * Metodo adicional para mostrar los datos de las naves comunes
 * @return
 */
	public String mostrarNavesComunes() {
		return toString() + ", Cantidad: " + cantidadNaves;
	}
}
