package clases;

import java.util.ArrayList;
/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Base {

	private String nombre;
	private int nivel;
	ArrayList<Escuadron> escuadrones;
	/**
	 * Clase nave con un unico constructor
	 * @param nombre
	 */
	public Base(String nombre) {
		this.nombre = nombre;
		this.escuadrones = new ArrayList<Escuadron>();
		this.nivel = escuadrones.size()/2;
	}
	/**
	 * 
	 * @return devuelve el nombre de la base
	 */
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * 
	 * @return devuelve el nivel de la base
	 */
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	/**
	 * 
	 * @return devuelve el arrayList de escuadron 
	 */
	public ArrayList<Escuadron> getEscuadrones() {
		return escuadrones;
	}
	public void setEscuadrones(Escuadron escuadron) {
		this.escuadrones.add(escuadron);
	}
	/**
	 * Metodo para listar correctamente los escuadrones de cada base
	 */
	public void listarEscuadrones() {
		for (int i=0;i<escuadrones.size();i++) {
			if(escuadrones.get(i) !=null) {
				System.out.println(escuadrones.get(i).getNombre());
			}
		}
	}
	/**
	 * Metodo para mostrar todos los datos de la base
	 */
	public void mostrarTodo() {
		System.out.println(toString());
		listarEscuadrones();
	}
	@Override
	public String toString() {
		return "Base " + nombre + ", nivel:" + nivel + ", escuadrones:";
	}
	
	
}
