package clases;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public class JuegoNaves {

	ArrayList<Escuadron> listaEscuadrones;
	ArrayList<NaveComun> listaNavesComunes;
	ArrayList<NaveLider> listaNavesLideres;
	ArrayList<Base> listaBases;

	/**
	 * Constructor de JuegoNaves que inicializa los arrays de las 4 clases
	 */
	public JuegoNaves() {
		this.listaEscuadrones = new ArrayList<Escuadron>();
		this.listaNavesComunes = new ArrayList<NaveComun>();
		this.listaNavesLideres = new ArrayList<NaveLider>();
		this.listaBases = new ArrayList<Base>();
	}

	/**
	 * Metodo para dar de alta a las naves lider
	 * 
	 * @param lider
	 * @param coste
	 * @param vida
	 * @param modelo
	 */
	public void altaNaveLider(String lider, int coste, int vida, String modelo) {
		if (!existeNaveLider(lider)) {
			NaveLider nuevaNave = new NaveLider(lider, coste, vida, modelo);
			listaNavesLideres.add(nuevaNave);
			System.out.println("Nave lider registrada correctamente");
		} else {
			System.out.println("La nave ya esta registrada");
		}
	}

	/**
	 * Metodo para dar de alta a las naves comunes
	 * 
	 * @param coste
	 * @param vida
	 * @param modelo
	 * @param cantidadNaves
	 */
	public void altaNaveComun(int coste, int vida, String modelo, int cantidadNaves) {
		if (!existeNaveComun(modelo)) {
			NaveComun nuevaNave = new NaveComun(modelo, coste, vida, cantidadNaves);
			listaNavesComunes.add(nuevaNave);
			System.out.println("Nave comun registrada correctamente");
		} else {
			System.out.println("La nave ya esta registrada");
		}
	}

	/**
	 * Metodo para dar de alta a los escuadrones
	 * 
	 * @param nombre
	 * @param color
	 * @param tamanio
	 * @param nombreLider
	 * @param modeloNaveComun
	 */
	public void altaEscuadron(String nombre, String color, int tamanio, String nombreLider, String modeloNaveComun) {
		if (!existeNaveComun(nombre)) {
			Escuadron nuevoEscuadron = new Escuadron(nombre, color, tamanio);
			if (buscarNaveLider(nombreLider) != null) {
				nuevoEscuadron.setNaveLider(buscarNaveLider(nombreLider));
			} else {
				System.out.println("No se ha encontrado ningun Lider con nombre " + nombreLider);
			}
			if (buscarNaveComun(modeloNaveComun) != null) {
				nuevoEscuadron.rellenarNaves(buscarNaveComun(modeloNaveComun));
				for (NaveComun naveComun : listaNavesComunes) {
					if (naveComun != null && naveComun.getModelo().equalsIgnoreCase(modeloNaveComun)) {
						naveComun.actualizarCantidadNaves(tamanio);
					}
				}
			} else {
				System.out.println("No se ha encontrado ninguna nave con modelo " + modeloNaveComun);
			}
			listaEscuadrones.add(nuevoEscuadron);
			System.out.println("Escuadron registrado correctamente");
		} else {
			System.out.println("El escuadron con nombre " + nombre + " ya esta registrado");
		}
	}

	/**
	 * Metodo para dar de alta a las bases
	 * 
	 * @param nombre
	 */
	public void altaBase(String nombre) {
		if (!existeBase(nombre)) {
			Base nuevaBase = new Base(nombre);
			listaBases.add(nuevaBase);
			System.out.println("Base registrada correctamente");
		} else {
			System.out.println("La base ya esta registrada");
		}
	}

	/**
	 * Metodo para comprobar si existe la nave lider
	 * 
	 * @param lider
	 * @return
	 */
	public boolean existeNaveLider(String lider) {
		for (NaveLider naveLider : listaNavesLideres) {
			if (naveLider != null && naveLider.getLider().equalsIgnoreCase(lider)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para comprobar si existe la nave comun
	 * 
	 * @param modelo
	 * @return
	 */
	public boolean existeNaveComun(String modelo) {
		for (NaveComun naveComun : listaNavesComunes) {
			if (naveComun != null && naveComun.getModelo().equalsIgnoreCase(modelo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para comprobar si existe el escuadron
	 * 
	 * @param nombre
	 * @return
	 */
	public boolean existeEscuadron(String nombre) {
		for (Escuadron escuadron : listaEscuadrones) {
			if (escuadron != null && escuadron.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para comprobar si existe la base
	 * 
	 * @param base
	 * @return
	 */
	public boolean existeBase(String base) {
		for (Base bases : listaBases) {
			if (bases != null && bases.getNombre().equalsIgnoreCase(base)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para listar las naves lideres
	 */
	public void listarNavesLideres() {
		for (int i = 0; i < listaNavesLideres.size(); i++) {
			if (listaNavesLideres.get(i) != null) {
				System.out.println(listaNavesLideres.get(i));
			}
		}
	}

	/**
	 * Metodo para listar las naves comunes
	 */
	public void listarNavesComunes() {
		for (int i = 0; i < listaNavesComunes.size(); i++) {
			if (listaNavesComunes.get(i) != null) {
				System.out.println(listaNavesComunes.get(i).mostrarNavesComunes());
			}
		}
	}

	/**
	 * Metodo para listar los escuadrones
	 */
	public void listarEscuadrones() {
		for (int i = 0; i < listaEscuadrones.size(); i++) {
			if (listaEscuadrones.get(i) != null) {
				listaEscuadrones.get(i).mostrarTodo();
			}
		}
	}

	/**
	 * Metodo para listar las bases
	 */
	public void listarBases() {
		for (int i = 0; i < listaBases.size(); i++) {
			if (listaBases.get(i) != null) {
				listaBases.get(i).mostrarTodo();
			}
		}
	}

	/**
	 * Metodo para buscar nave lider
	 * 
	 * @param nombreLider
	 * @return
	 */
	public NaveLider buscarNaveLider(String nombreLider) {
		for (NaveLider naveLider : listaNavesLideres) {
			if (naveLider != null && naveLider.getLider().equalsIgnoreCase(nombreLider)) {
				return naveLider;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar naves Comunes
	 * 
	 * @param modelo
	 * @return
	 */
	public NaveComun buscarNaveComun(String modelo) {
		for (NaveComun naveComun : listaNavesComunes) {
			if (naveComun != null && naveComun.getModelo().equalsIgnoreCase(modelo)) {
				return naveComun;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar Escuadron
	 * 
	 * @param nombre
	 * @return
	 */
	public Escuadron buscarEscuadron(String nombre) {
		for (Escuadron escuadron : listaEscuadrones) {
			if (escuadron != null && escuadron.getNombre().equalsIgnoreCase(nombre)) {
				return escuadron;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar base
	 * 
	 * @param nombre
	 * @return
	 */
	public Base buscarBase(String nombre) {
		for (Base base : listaBases) {
			if (base != null && base.getNombre().equalsIgnoreCase(nombre)) {
				return base;
			}
		}
		return null;
	}

	/**
	 * Metodo para eliminar naves lideres
	 * 
	 * @param lider
	 */
	public void eliminarNaveLider(String lider) {
		Iterator<NaveLider> iteradorNavesLider = listaNavesLideres.iterator();
		while (iteradorNavesLider.hasNext()) {
			NaveLider naveLider = iteradorNavesLider.next();
			if (naveLider.getLider().equals(lider)) {
				iteradorNavesLider.remove();
				System.out.println("Nave con lider " + lider + " eliminada");
			}
		}
	}

	/**
	 * Metodo para eliminar naves comunes
	 * 
	 * @param modelo
	 */
	public void eliminarNaveComun(String modelo) {
		Iterator<NaveComun> iteradorNavesComunes = listaNavesComunes.iterator();
		while (iteradorNavesComunes.hasNext()) {
			NaveComun naveComun = iteradorNavesComunes.next();
			if (naveComun.getModelo().equals(modelo)) {
				iteradorNavesComunes.remove();
				System.out.println("Nave con modelo " + modelo + " eliminada");
			}
		}
	}

	/**
	 * Metodo para eliminar escuadrones
	 * 
	 * @param nombre
	 */
	public void eliminarEscuadron(String nombre) {
		Iterator<Escuadron> iteradorEscuadrones = listaEscuadrones.iterator();
		while (iteradorEscuadrones.hasNext()) {
			Escuadron escuadron = iteradorEscuadrones.next();
			if (escuadron.getNombre().equals(nombre)) {
				iteradorEscuadrones.remove();
				System.out.println("Escuadron con nombre " + nombre + " eliminada");
			}
		}
	}

	/**
	 * Metodo para eliminar las bases
	 * 
	 * @param nombre
	 */
	public void eliminarBase(String nombre) {
		Iterator<Base> iteradorBases = listaBases.iterator();
		while (iteradorBases.hasNext()) {
			Base bases = iteradorBases.next();
			if (bases.getNombre().equals(nombre)) {
				iteradorBases.remove();
				System.out.println("Escuadron con nombre " + nombre + " eliminada");
			}
		}
	}

	/**
	 * Metodo para asignar un escuadron a una base
	 * 
	 * @param nombreEscuadron
	 * @param nombreBase
	 */
	public void asignarEscuadron(String nombreEscuadron, String nombreBase) {
		if (listaEscuadrones.contains(buscarEscuadron(nombreEscuadron))) {
			if (listaBases.contains(buscarBase(nombreBase))) {
				buscarBase(nombreBase).setEscuadrones(buscarEscuadron(nombreEscuadron));
				System.out.println(
						"Se ha asignado correctamente el escuadron " + nombreEscuadron + " a la base " + nombreBase);
			} else {
				System.out.println("La base introducida no existe");
			}
		} else {
			System.out.println("el escuadron introducido no existe");
		}
	}

	/**
	 * Metodo para asignar nave lider y aves comunes al escuadron
	 * 
	 * @param nombreEscuadron
	 * @param nombreLider
	 * @param modeloNaveComun
	 */
	public void rellenarEscuadron(String nombreEscuadron, String nombreLider, String modeloNaveComun) {
		if (listaEscuadrones.contains(buscarEscuadron(nombreEscuadron))) {
			if (listaNavesLideres.contains(buscarNaveLider(nombreLider))) {
				buscarEscuadron(nombreEscuadron).setNaveLider(buscarNaveLider(nombreLider));
				System.out.println("Lider asignado correctamente");
			} else {
				System.out.println("No se ha encontrado ningun Lider con nombre " + nombreLider);
			}
			if (listaNavesComunes.contains(buscarNaveComun(modeloNaveComun))) {
				buscarEscuadron(nombreEscuadron).rellenarNaves(buscarNaveComun(modeloNaveComun));
				System.out.println("Naves asignadas correctamente");
				for (NaveComun naveComun : listaNavesComunes) {
					if (naveComun != null && naveComun.getModelo().equalsIgnoreCase(modeloNaveComun)) {
						naveComun.actualizarCantidadNaves(buscarEscuadron(nombreEscuadron).getTamanio());

					}
				}
			} else {
				System.out.println("No se ha encontrado ninguna nave con modelo " + modeloNaveComun);
			}
		} else {
			System.out.println("el escuadron introducido no existe");
		}
	}
}