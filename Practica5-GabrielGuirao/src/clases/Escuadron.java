package clases;
/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Escuadron {

	private String nombre;
	private int tamanio;
	private String color;
	private NaveLider naveLider;
	private NaveComun[] navesComunes;

	/**
	 * Clase escuadron con un solo constructor
	 * 
	 * @param nombre
	 * @param color
	 * @param tamanio
	 */
	public Escuadron(String nombre, String color, int tamanio) {
		this.tamanio = tamanio;
		this.nombre = nombre;
		this.color = color;
	}

	/**
	 * 
	 * @return devuelve el nombre del escuadron
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return devuelve el tamanio del escuadron
	 */
	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}

	/**
	 * 
	 * @return devuelve el color del escuadron
	 */
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * 
	 * @return devuelve un objeto de nave Lider
	 */
	public NaveLider getNaveLider() {
		return naveLider;
	}

	public void setNaveLider(NaveLider naveLider) {
		this.naveLider = naveLider;
	}

	/**
	 * 
	 * @return devuelve un array de naves Comunes
	 * 
	 */
	public NaveComun[] getNavesComunes() {
		return navesComunes;
	}

	public void setNavesComunes(NaveComun[] navesComunes) {
		this.navesComunes = navesComunes;
	}

	/**
	 * Metodo para rellenar el array navesComunes[]
	 * 
	 * @param naveComun
	 */
	public void rellenarNaves(NaveComun naveComun) {
		if ((naveComun.getCantidadNaves() - (tamanio - 1)) >= 0) {
			this.navesComunes = new NaveComun[tamanio - 1];

		} else {
			setTamanio(naveComun.getCantidadNaves() + 1);
			this.navesComunes = new NaveComun[naveComun.getCantidadNaves()];
			System.out.println(
					"Tamano del escuadron reducido a " + getTamanio() + " debido a la escasez de naves suficientes");
		}
		for (int i = 0; i < navesComunes.length; i++) {
			navesComunes[i] = naveComun;
		}
	}

	/**
	 * Metodo adicional para mostrar las naves Comunes
	 */
	private void mostrarNaves() {
		boolean escuadronVacio = false;
		for (int i = 0; i < navesComunes.length; i++) {
			if (navesComunes[i] == null) {
				escuadronVacio = true;
			} else {
				System.out.println(navesComunes[i]);
			}
		}
		if (escuadronVacio) {
			System.out.print("Este escuadron esta vacio\n");
		}
	}

	/**
	 * Metodo que se utiliza para mostrar la nave lider
	 * 
	 * @return
	 */
	private String mostrarNaveLider() {
		if (naveLider == null) {
			return "";
		}
		return naveLider.toString();
	}

	@Override
	public String toString() {
		return "El escuadron con nombre " + nombre + ", tamanio " + tamanio + ", color " + color
				+ "\ncontiene las siguientes naves:\n" + mostrarNaveLider();
	}

	/**
	 * Metodo adicional para mostrar correctamente todos los datos de escuadron
	 */
	public void mostrarTodo() {
		System.out.println(toString());
		mostrarNaves();
	}

}
