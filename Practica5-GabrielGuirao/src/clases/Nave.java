package clases;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public abstract class Nave {

	String modelo;
	int coste;
	int vida;

	/**
	 * Clase nave con un solo constructor
	 * 
	 * @param modelo
	 * @param coste
	 * @param vida
	 */
	public Nave(String modelo, int coste, int vida) {
		this.modelo = modelo;
		this.coste = coste;
		this.vida = vida;
	}

	/**
	 * 
	 * @return devuelve el modelo de la nave
	 */
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	/**
	 * 
	 * @return devuelve el coste de la nave
	 */
	public int getCoste() {
		return coste;
	}

	public void setCoste(int coste) {
		this.coste = coste;
	}

	/**
	 * 
	 * @return devuelve la vida de la nave
	 */
	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	@Override
	public String toString() {
		return "Nave [modelo=" + modelo + ", coste=" + coste + ", vida=" + vida;
	}

}
