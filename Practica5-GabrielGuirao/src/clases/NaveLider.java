package clases;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public final class NaveLider extends Nave {

	private String lider;

	/**
	 * Clase Nave lider con un solo constructor
	 * 
	 * @param lider
	 * @param coste
	 * @param vida
	 * @param modelo
	 */
	public NaveLider(String lider, int coste, int vida, String modelo) {
		super(modelo, coste, vida);
		this.lider = lider;
	}

	/**
	 * 
	 * @return devuelve el nombre del lider del escuadron
	 */
	public String getLider() {
		return lider;
	}

	public void setLider(String lider) {
		this.lider = lider;
	}

	@Override
	public String toString() {
		return "Nave Lider: Lider: " + lider + ", Modelo: " + modelo + ", Coste: " + coste + ", Vida: " + vida;
	}

}
